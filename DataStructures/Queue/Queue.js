const LinkedList = require('./SimplifiedLinkedList');

class Queue{
  constructor(){
    this.queue = new LinkedList();
    this.size = 0;
  }

  enqueue(){
    //Queue adds a node to the end of the list
    this.queue.addToTail(data);
    //Increment the size by 1
    this.size++;
    console.log(`Added ${data}! Queue size is now ${this.size}`);
  }

  dequeue(){
    //Queue removes from the head
    //First In, First Out
    const data = this.queue.removeHead();
    //Update the size
    this.size--;
    console.log(`Removed ${data}! Queue size is now ${this.size}`);

    //return the data removed
    return data;
  }
}