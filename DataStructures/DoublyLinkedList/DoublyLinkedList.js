const DoubleNode = require('./DoubleNode');

class DoublyLinkedList {
  // Create your constructor below:
  constructor(){
    this.head = null;
    this.tail = null;
  }

  addToHead(data){
    const newHead = new DoubleNode(data);
    const currentHead = this.head;
    if(currentHead){
      newHead.setNextNode(currentHead);
      currentHead.setPreviousNode(newHead);
    }
    this.head = newHead;
    if(!this.tail){
      this.tail = newHead;
    }
  }

  addToTail(data){
    const newTail = new DoubleNode(data);
    const currentTail = this.tail;
    // Update the tail pointers
    if(currentTail){
      currentTail.setNextNode(newTail);
      newTail.setPreviousNode(currentTail);
    }
    // Set/Update the new tail
    this.tail = newTail;
    // Check if the list is empty;
    if(!this.head){
      // update the head as well if so
      this.head = newTail;
    }
  }

  removeHead(){
    const removedHead = this.head;
    if(!removedHead){
      return;
    }
    // We do have a current head
    // Set the 2nd node in the list to the new head
    this.head = removedHead.getNextNode();

    // There's more than one item in the list
    if(this.head){
      // Set the previous pointer to null
      this.head.setPreviousNode(null);
    }

    if(removedHead === this.tail){
      this.removeTail();
    }

    return removedHead.data;
  }

  removeTail(){
    const removedTail = this.tail;
    if(!removedTail){
      return;
    }

    //We do have a current tail
    // set the previous node to the new tail
    this.tail = removedTail.getPreviousNode();

    if(this.tail){
      this.tail.setNextNode(null);
    }

    if(removedTail === this.head){
      this.removeHead();
    }

    return removedTail.data;
  }

  removeByData(data){
    let nodeToRemove;
    let currentNode = this.head;

    while(currentNode !== null){
      if(currentNode.data === data){
        //Found the node to remove
        nodeToRemove = currentNode;
        break;
      }
      currentNode = currentNode.getNextNode();
    }

    if(!nodeToRemove){
      return null;
    }

    if(nodeToRemove === this.head){
      this.removeHead();
    }
    else if(nodeToRemove === this.tail){
      this.removeTail();
    }
    else{
      const nextNode = nodeToRemove.getNextNode();
      const prevNode = nodeToRemove.getPreviousNode();
      prevNode.setNextNode(nextNode);
      nextNode.setPreviousNode(prevNode);
    }

    return nodeToRemove;
  }

  removeByDataReverse(data) {
    let nodeToRemove;
    let currentNode = this.tail;

    while (currentNode !== null) {
      if (currentNode.data === data) {
        nodeToRemove = currentNode;
        break;
      }
      currentNode = currentNode.getPreviousNode();
    }

    if (!nodeToRemove) {
      return null;
    }

    if (nodeToRemove === this.head) {
      this.removeHead();
    } else if (nodeToRemove === this.tail) {
      this.removeTail();
    } else {
      const nextNode = nodeToRemove.getNextNode();
      const previousNode = nodeToRemove.getPreviousNode();
      nextNode.setPreviousNode(previousNode);
      previousNode.setNextNode(nextNode);
    }
    return nodeToRemove;
  }
  
  printList() {
    let currentNode = this.head;
    let output = '<head> ';
    while (currentNode !== null) {
      output += currentNode.data + ' ';
      currentNode = currentNode.getNextNode();
    }
    output += '<tail>';
    console.log(output);
  }
}

module.exports = DoublyLinkedList;