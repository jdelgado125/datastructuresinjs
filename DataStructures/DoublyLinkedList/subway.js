const DoublyLinkedList = require('./DoublyLinkedList.js');

//Create the doubly linked list
let subway = new DoublyLinkedList();

// Add to head
subway.addToHead('TimesSquare');
subway.addToHead('GrandCentral');
subway.addToHead('CentralPark');
subway.printList();

// Add to Tail
subway.addToTail('PennStation');
subway.addToTail('WallStreet');
subway.addToTail('BrooklynBridge');
subway.printList();

// Remove stops
subway.removeByData('TimesSquare');
subway.printList();

//Remove in reverse
subway.removeByDataReverse('GrandCentral');
subway.printList();