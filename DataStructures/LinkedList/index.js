const LinkedList = require('./LinkedList');

const link = new LinkedList();
// link.addToTail(8);
// link.printList();
// link.addToTail(9);
// link.addToTail(10);
// link.addToHead(7);
// link.printList();
// let removed = link.removeAtIndex(3);
// console.log(`Removed: ${removed}`);
// link.printList();

for (let index = 0; index < 10; index++) {
  link.addToTail(index);
}

// let reverse = link.reverseList();
// console.log(reverse);

swapNodes(link, 2, 5);
link.printList();

function swapNodes(list, data1, data2) {
  let node1 = list.head;
  let node2 = list.head;
  let node1Prev = null;
  let node2Prev = null;

  if(data1 === data2){
    console.log('Elements are the same - no swap to be made');
    return;
  }

  while(node1 !== null){
    if(node1.data === data1){
      break;
    }
    node1Prev = node1;
    node1 = node1.getNextNode();
  }

  while(node2 !== null){
    if(node2.data === data2){
      break;
    }
    node2Prev = node2;
    node2 = node2.getNextNode();
  }

  if(node1 === null || node2 === null){
    console.log('No swap is possible - one or more elements are not in the list');
    return;
  }

  if(node1Prev === null){
    list.head = node2;
  }else{
    node1Prev.setNextNode(node2);
  }

  if(node2Prev === null){
    list.head = node1;
  }else{
    node2Prev.setNextNode(node1);
  }

  let temp = node1.getNextNode();
  node1.setNextNode(node2.getNextNode());
  node2.setNextNode(temp);
}

// Complete this function
const nthLastNode = ( linkedList, n) => {
  let nthLastNode = null;
  let tailPointer = linkedList.head;
  let count = 0;

  while(tailPointer){
    tailPointer = tailPointer.getNextNode();
    if(count >= n){
      if(!nthLastNode){
        nthLastNode = linkedList.head;
      }
      
      nthLastNode = nthLastNode.getNextNode();
    }
    count++;
  }
  return nthLastNode;
};

// Test your function yourself:
console.log(nthLastNode(testLinkedList, 4));
