const Node = require('./Node');

class LinkedList {
  constructor() {
    this.head = null;
    this.size = 0;
  }

  addToHead(data) {
    const newHead = new Node(data);
    const currentHead = this.head;
    this.head = newHead;
    this.size++;
    if (currentHead) {
      this.head.setNextNode(currentHead);
    }
  }

  addToTail(data) {
    let tail = this.head;
    if (!tail) {
      this.head = new Node(data);
    } else {
      while (tail.getNextNode() !== null) {
        tail = tail.getNextNode();
      }
      tail.setNextNode(new Node(data));
    }
    this.size++;
  }

  removeHead() {
    const removedHead = this.head;
    if (!removedHead) {
      return;
    }
    this.head = removedHead.getNextNode();
    this.size--;
    return removedHead.data;
  }

  removeAtIndex(index){
    if(index < 0 || index >= this.size){
      throw new Error('Index is out of bounds.');
    }

    let count = 0;
    let currentNode = this.head;
    while(count < index - 1){
      currentNode = currentNode.getNextNode();
      count++;
    }
    currentNode.next = currentNode.next.next;
    return currentNode.data;
  }

  printList() {
    let currentNode = this.head;
    let output = '<head> ';
    while (currentNode !== null) {
      output += currentNode.data + ' ';
      currentNode = currentNode.getNextNode();
    }
    output += '<tail>';
    console.log(output);
  }

  reverseList(){
    let prev = null;

    while(this.head !== null){
      let nextNode = this.head.next;
      this.head.next = prev;
      prev = this.head;
      this.head = nextNode;
    }
    return prev;
  }

}

module.exports = LinkedList;