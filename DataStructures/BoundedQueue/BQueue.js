const LinkedList = require('../Queue/SimplifiedLinkedList');

class BoundedQueue{
  constructor(maxSize = Infinity){
    this.queue = new LinkedList();
    this.size = 0;
    this.maxSize = maxSize;
  }

  hasRoom(){
    return this.size < this.maxSize;
  }

  isEmpty(){
    if(this.size === 0){
      return true;
    }

    return false;
  }

  enqueue(){
    if(this.hasRoom()){
      //Queue adds a node to the end of the list
      this.queue.addToTail(data);
      //Increment the size by 1
      this.size++;
      console.log(`Added ${data}! Queue size is now ${this.size}`);
    }else{
      throw new Error('Queue is full!');
    }
  }

  dequeue(){
    //Queue removes from the head
    //First In, First Out
    if(!this.isEmpty()){
      const data = this.queue.removeHead();
      //Update the size
      this.size--;
      console.log(`Removed ${data}! Queue size is now ${this.size}`);
  
      //return the data removed
      return data;
    }else{
      throw new Error('Queue is empty!');
    }
  }
}